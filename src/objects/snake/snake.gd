extends Node2D

var directionX = 1
var directionY = 0
var cell_size = 32
var skip = true

var seg = preload("res://objects/snake/segment.tscn")
onready var current_pos = position
var segments = []

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	new_segment()
	pass # Replace with function body.

func _process(delta):
	get_inputs()

func new_segment():
	var segment = seg.instance()
	segment.snake = self
	segment.position = current_pos
	segments.append(segment)
	add_child(segment)
	
func get_inputs():
	if Input.is_action_just_pressed("ui_right"):
		directionX = 1
		directionY = 0
	elif Input.is_action_just_pressed("ui_left"):
		directionX = -1
		directionY = 0
	elif Input.is_action_just_pressed("ui_up"):
		directionX = 0
		directionY = -1
	elif Input.is_action_just_pressed("ui_down"):
		directionX = 0
		directionY = 1
		
	if Input.is_action_just_pressed("ui_accept"):
		skip = true


func update_position():
	
	current_pos.x += directionX * cell_size
	current_pos.y += directionY * cell_size
	new_segment()
	if !skip:
		segments[0].queue_free()
		segments.remove(0)
	skip = false
