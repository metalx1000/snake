extends Area2D

var snake

func _ready():
	pass # Replace with function body.



func collide(area):
	if area.is_in_group("pellets"):
		area.queue_free()
		snake.skip = true
		get_tree().get_current_scene().get_node("Timer").wait_time *= .9
		get_tree().get_nodes_in_group("levels")[0].create_pellet()
	else:
		get_tree().reload_current_scene()
