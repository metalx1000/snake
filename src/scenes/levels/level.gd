extends Node2D

var pellets = preload("res://objects/pellet/pellet.tscn")
var snakes = preload("res://objects/snake/snake.tscn")

var cell_size = 32

func _ready():
	create_pellet()
	create_snake()

func snake_update():
	var seg = get_tree().get_nodes_in_group("segments")[0]
	
	if seg.position.x < 0 || seg.position.y < 0 || seg.position.x >  700 || seg.position.y > 420:
		get_tree().reload_current_scene()
	
	for snake in get_tree().get_nodes_in_group("snakes"):
		snake.update_position()

func create_snake():
	var snake = snakes.instance()
	snake.position = random_position()/2
	add_child(snake)
	
	
func create_pellet():
	var pellet = pellets.instance()
	pellet.position = random_position()
	add_child(pellet)
	
func random_position():
	randomize()
	var w = get_viewport().size.x
	var h = get_viewport().size.y
	var x = w/cell_size
	var y = h/cell_size
	
	x = floor(rand_range(1,x))
	y = floor(rand_range(1,y))
	
	return Vector2(x*cell_size,y*cell_size)
	
func _input(event):
	if event.is_action_released("music_pause"):
		if $music.playing:
			$music.stop()
		else:
			$music.play()
			
		
	if event.is_action("ui_cancel"):
		get_tree().quit()
